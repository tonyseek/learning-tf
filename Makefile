.PHONY: help setup shell clean

PYTHON = venv/bin/python
PYTHON_GLOBAL = python3
PYTHON_PIP = $(PYTHON) -m pip
PYTHON_SETUP = venv/.$(shell shasum -a 256 requirements.txt | tr -s ' ' '-')
PYTHON_VERIFY = import tensorflow as tf;print(tf.reduce_sum(tf.random.normal([1000, 1000])))

help:
	@printf '%s\n' "$(_VENV_PIP_DIGEST)"
	@printf >&2 'Commands:\n'
	@printf >&2 '  setup    Setup virtualenv and install requirements.\n'
	@printf >&2 '  shell    Spawn a shell.\n'

setup:
	rm -f $(PYTHON_SETUP)
	$(MAKE) $(PYTHON_SETUP)

shell: $(PYTHON_SETUP)
	@. venv/bin/activate && $(shell printenv SHELL) || true

clean:
	rm -rf venv

$(PYTHON_SETUP): $(PYTHON) requirements.txt
	$(PYTHON_PIP) install -r requirements.txt
	$(PYTHON) -c "$(PYTHON_VERIFY)" > $(PYTHON_SETUP)

$(PYTHON):
	$(PYTHON_GLOBAL) -m venv venv
	$(PYTHON_PIP) install -U pip
